import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Login me",
      home: Scaffold(
        appBar: AppBar(title: Text('Register')),
        body: LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String firstname;
  late String lastname;
  late String birthday;
  late String address;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              fieldEmailAddress(),
              Container(margin: EdgeInsets.only(top: 20.0),),
              fieldFirstname(),
              Container(margin: EdgeInsets.only(top: 20.0),),
              fieldLastName(),
              Container(margin: EdgeInsets.only(top: 20.0),),
              fieldBirthday(),
              Container(margin: EdgeInsets.only(top: 20.0),),
              fieldAddress(),
              Container(margin: EdgeInsets.only(top: 20.0),),
              loginButton()
            ],
          ),
        )

    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Email'
      ),
      validator: (value) {
        if (!value!.contains('@')) {
          return 'Pls input valid email.';
        }
        return null;
      },
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }


  Widget fieldFirstname() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'FirstName'
      ),

      onSaved: (value) {
        firstname = value as String;
      },
    );
  }
  Widget fieldLastName() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'FirstName'
      ),

      onSaved: (value) {
        lastname = value as String;
      },
    );
  }
  Widget fieldBirthday() {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'BirthDay'
      ),

      onSaved: (value) {
        birthday = value as String;
      },
    );
  }
  Widget fieldAddress() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Address'
      ),

      onSaved: (value) {
        address = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('$emailAddress, Họ& chữ lót: $lastname, Tên: $firstname, Năm Sinh: $birthday, Địa chỉ: $address');
          }
        },
        child: Text('Register')
    );
  }
}